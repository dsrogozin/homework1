package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TinkoffCashabackServiceTest {
  @Test
  void oneTransaction() {
    // (A)rrange
    final TinkoffCashbackService service = new TinkoffCashbackService();
    final float cash = 301.0f;

    final int expected = 3;

    // (A)ct
    final int actual = service.cashbackСalculate(cash);

    // (A)ssert
    assertEquals(expected, actual);
  }

  @Test
  void manyTransaction() {
    // (A)rrange
    final TinkoffCashbackService service = new TinkoffCashbackService();
    final double cash = 301.0f;

    final int expected = 558;

    // (A)ct
    final int actual = service.cashbackСalculate(cash)
            + service.cashbackСalculate(cash + 55212);

    // (A)ssert
    assertEquals(expected, actual);
  }

  @Test
  void limitCheck() {
    // (A)rrange
    final TinkoffCashbackService service = new TinkoffCashbackService();
    final double cash = 311100;
    final int expected = 3000;

    // (A)ct
    final int actual = service.cashbackСalculate(cash);

    // (A)ssert
    assertEquals(expected, actual);
  }



  @Test
  void errorsCash() {
    // (A)rrange
    final TinkoffCashbackService service = new TinkoffCashbackService();
    final double cash = -2f;
    int month = 12;
    int year = 2021;
    final int expected = 0;

    // (A)ct
    final int actual = service.cashbackСalculate(cash);

    // (A)ssert
    assertEquals(expected, actual);
  }




}
