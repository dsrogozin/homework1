package org.example.service;


public class TinkoffCashbackService {
    private final int limit = 3000;
    private int receivedСashback = 0;


    public int cashbackСalculate(double cash) {

        double result = cash * 0.01F;
        receivedСashback += Math.round(result);
        //проверка потолка лимита
        if (receivedСashback > limit) {
            int overflow = receivedСashback - limit;
            receivedСashback = receivedСashback - overflow;
            result = result - overflow;
        }
        return (int) Math.round(result);
    }

}
